# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 07:08+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Printing/Printing/.project:17
msgid "Printing example"
msgstr "Пример печати"

#: app/examples/Printing/Printing/.project:18
msgid ""
"Printing example.\n"
"\n"
"This example shows how to print a text or an image."
msgstr ""
"Пример печати.\n"
"\n"
"В этом примере показано, как напечатать текст или изображение."

#: app/examples/Printing/Printing/.src/FMain.form:15
msgid "Text"
msgstr "Текст"

#: app/examples/Printing/Printing/.src/FMain.form:22
msgid "Load text file"
msgstr "Загрузить текстовый файл"

#: app/examples/Printing/Printing/.src/FMain.form:28
msgid "Rich text"
msgstr "Rich text"

#: app/examples/Printing/Printing/.src/FMain.form:41 app/examples/Printing/Printing/.src/FMain.form:76
msgid "Print"
msgstr "Печать"

#: app/examples/Printing/Printing/.src/FMain.form:47
msgid ""
"<h2>Hello!</h2>\n"
"This is a <b>text printing</b> <i>example</i>."
msgstr ""
"<h2>Привет!</h2>\n"
"Это <i>пример</i> <b>печати текста</b>."

#: app/examples/Printing/Printing/.src/FMain.form:59
msgid "Image"
msgstr "Изображение"

#: app/examples/Printing/Printing/.src/FMain.form:66
msgid "Load image file"
msgstr "Загрузить файл изображения"

