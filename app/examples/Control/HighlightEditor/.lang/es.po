#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: FEditor.class:201
msgid "HTML Highlighting Editor"
msgstr "Editor con resaltado HTML "

#: FEditor.class:206
msgid "Popup"
msgstr "Popup"

#: FEditor.class:210
msgid "Highlight HTML"
msgstr "Resaltado HTML"

#: FEditor.class:215
msgid "Highlight immediately"
msgstr "Resaltar instantáneamente"

#: FEditor.class:222
msgid "Quit"
msgstr "Salir"

#~ msgid "HTML highlighting with the editor control"
#~ msgstr "Resaltado HTML con el control editor"
